# HPC Tutorial

This repository contains sample code on how to run code on the [RWTH Compute Cluster](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/).

## What is a cluster

In the most basic sense a cluster is a collection of networked computers. 
The RWTH has actually [several clusters](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/fbd107191cf14c4b8307f44f545cf68a/).
The CLAIX-2018 cluster for example consists of ~1250 computers or 'nodes' with two 24-core Processors and 192GB of RAM per Node.

To connect to the cluster special login-nodes are reserved. 
These can be used to access the clusters and test the programs you intend to run.
Once you are sure that your program runs as intended you need to specify the required resources and can submit it to the scheduler.
The scheduler will then run the program on the requested hardware once it is available.

## Prerequisites

- In order to connect to the cluster an HPC-account ('Hochleistungsrechnen RWTH Aachen') is required.
You can create one [here](https://idm.rwth-aachen.de/selfservice/CreateAccount?2).
- You will connect to the cluster via ssh:
  - Unix machines should have this preinstalled
  - Window machines can use the [Git-Bash](https://git-scm.com/download/win) or the linux subsystem.
- Make sure you are on the RWTH network or VPN as connections to the cluster are only allowed from the university network.
- If you are unfamiliar with terminal shells such as bash or zsh a useful cheatsheet can be found [here](https://github.com/RehanSaeed/Bash-Cheat-Sheet).

## Connecting to the cluster
We will connect to the cluster via ssh. There are several available [login-nodes](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/0a23d513f31b4cf1849986aaed475789/).
For this tutorial we will be using ```login18-g-1.hpc.itc.rwth-aachen.de```.
To connect, open a terminal prompt (or the git-bash) and execute:

```shell
ssh <your_userid>@login18-g-1.hpc.itc.rwth-aachen.de
```
You will then be prompted for the password for your HPC-account.
If you want to skip the password step you can use [ssh-key authorization](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server).

## Getting code and data onto the cluster
For the purposes of this tutorial we will simply clone the git repository from the cluster. To do so, once you have connected to the cluster execute:
```shell
git clone https://git.rwth-aachen.de/lukas.fischer-wulf/hpc-tutorial.git
```

and change the directory by executing
```shell
cd hpc-tutorial
``` 
Git repositorys can then be synced via the normal pull and push commands. If you want to synchronize private repositories, [ssh-key authorization](https://git.rwth-aachen.de/help/user/ssh.md) is advised.

Alternatively data can be synchronized by using [rsync](https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories).
If you need to transfer large volumes of data, special [data transfer nodes](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/dbd3240c521d4595bec6b08667c4d42c/) are availible with bandwidths of ```2 x 40Gbit/s```.

## Testing programs on the login nodes
### Getting the right software on the cluster
To run the programs we need to have the right software availible.
The cluster makes a [variety of software](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/022b08d321fb45609bedb391ad46ef42/) availible via the [module system](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/417f822b8a7849eb8c9c2753045ad67f/).

The available software can be listed:
```shell
module avail
```

For example, we can load python by executing:
```shell
module load python
```

Loaded software can be listed with:
```shell
module list
```
Furthermore software can be 'uninstalled' using:
```shell
module unload [modulename]
```

### Testing your program

Once you have the required software availible it is time to test your program.
Having loaded python as an example you can execute:
```shell
python3 code/fibs.py 
```

## Submitting programs to the scheduler

To let your program run on the cluster you need to submit it to the SLURM scheduler.
Furthermore the required resources need to be specified. 
All of this is done via a batch script.
An example is the ``fibs.slrm`` file:

```shell
#!/usr/local_rwth/bin/zsh
### Task name
#SBATCH --job-name=fibonacci-fun

### Number of requested nodes
#SBATCH --ntasks=1

### Number of cpus per node
#SBATCH --cpus-per-task=1

### Amount of RAM per cpu
#SBATCH --mem-per-cpu=1G

### Maximum runtime
#SBATCH -t 00:10:00

### Output file
#SBATCH -o fibs.%J.out

### Mail notification configuration
#SBATCH --mail-type=ALL
#SBATCH --mail-user=yourname@example.com

module load python

python3 fibs.py
```

The first line specifies the shell used to execute the script.
SLURM variables are set after ``#SBATCH`` and must come before any uncuommented lines.

In the example above 1 cpu is requested on 1 node and 1GB of RAM is requested per cpu.
A walltime is set for 10 minutes as to avoid potentially endless loops consuming unnecessary resources.
The file for output on STDOUT and STDERR is specified to ``fibs.<job-id>.out`` and finally email notifications are activated.
An overview of possible SLURM commands and variables can be found [here](https://hpc-wiki.info/hpc/SLURM).

After all requirements are specified the program to be executed is launched.

To find the memory requirements for the sample program you can use:
```shell
r_memusage --vm python3 code/fibs.py
```

To submit it to the scheduler we need to move to the relevant directory. 
```shell
cd code/
```
We can then use the sbatch command to submit the script: 
```shell
sbatch fibs.slrm
```

To see the status of your submitted jobs, use:
```shell
squeue -u $USER
```

If you want to cancel a job use:
```shell
scancel <jobid>
```
The cluster documentation also provides [best practices](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/17c1d5db4efc45f1bcb85752313ba70a/) on SLURM usage. 

## Parallelization
If we want to use more than one core we can use [openMP](https://www.openmp.org/) to parralellize tasks on several cores.
NumPy, SciPy and Pandas support this out of the box. 
For other applications you'll have to code it yourself. 

If you require more than one node you likely need to look into [MPI](https://www.open-mpi.org/).

## Usage Quotas
To ensure fair usage of the cluster, [usage quotas](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/45825b06afb647e194be4a5b9f5b8768/) are set. 
Normal students have a quota of ``500 corehours`` per month.
For theses ``48000 corehours`` can be requested.
Larger quotas for other projects are also available.

To check your usage use:
```shell
r_wlm_usage
```
and
```shell
r_wlm_usage -q
``` 

Exactly how usage is calculated is specified [here](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/090b27dc31484f3c833957978b039b55/).

## Useful Links
- [HPC-Wiki](https://hpc-wiki.info/)
- [Cluster-Documentation](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/)
- [Bash-Cheatsheet](https://github.com/RehanSaeed/Bash-Cheat-Sheet)
- [MIT Course on Shell and other useful programming tools](https://missing.csail.mit.edu/)