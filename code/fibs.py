import logging
import time
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)

def get_fib_no(number: int):
    if number in {0, 1}:
        return number
    else:
        return get_fib_no(number - 1) + get_fib_no(number - 2)

def get_fibs(number: int):
    fibs = [0,1]
    if number in {0,1}:
        return fibs[:number+1]
    else:
        while len(fibs) < number:
            fibs.append(fibs[-1] + fibs[-2])
        return fibs

if __name__ == '__main__':
    n = 36
    log.info(f'Calculating {n} fibonacci numbers recursively:')
    start = time.time()
    fibs = [get_fib_no(i) for i in range(n)]
    log.info(fibs)
    log.debug(f'Took {time.time() - start}s')
    log.info(f'Calculating {n} fibonacci numbers iteratively:')
    start_iter = time.time()
    fibs = get_fibs(n)
    log.info(fibs)
    log.debug(f'Took {time.time() - start_iter}s')
